package ru.csu.springweb.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.csu.springweb.dto.Poll;
import ru.csu.springweb.dto.Qst;
import ru.csu.springweb.mapper.PollMapper;
import ru.csu.springweb.mapper.QstMapper;
import ru.csu.springweb.repository.PollRepository;
import ru.csu.springweb.repository.QstRepository;
import ru.csu.springweb.service.PollService;
import javax.transaction.Transactional;



import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PollServiceImpl implements PollService {

    private final PollRepository pollRepository;
    private final PollMapper pollMapper;

    private final QstMapper questionMapper;
    private final QstRepository questionsRepository;
    @Override
    public void createPoll(Poll poll) {
        pollRepository.save(pollMapper.mapSource(poll));
    }

    @Override
    public void deletePoll(long id) {
        pollRepository.deleteById(id);

    }

    @Override
    public ru.csu.springweb.model.Poll getPoll(long id) {
        return pollRepository.findById(id)
                .map(p -> ru.csu.springweb.model.Poll
                        .builder()
                        .id(p.getId())
                        .pollName(p.getPollName())
                        .active(p.getActive())
                        .startDate(p.getStartDate())
                        .endDate(p.getEndDate())
                        .build())
                .orElseThrow(() -> {
                    throw new EntityNotFoundException("Запрашеваемый ресурс не найден");
                });
    }


    @Override
    @Transactional
    public void addQuestionToPoll(long pollId) {
        throw new EntityNotFoundException("Неверный запрос в базу данных");
    }

    @Override
    public void addQuestionToPoll(long pollId, Qst question) {
        var poll = pollRepository.findById(pollId).orElseThrow(() -> {
            throw new EntityNotFoundException("");
        });

        var ques = questionMapper.mapTarget(question);

        ques.setPoll(poll);
        questionsRepository.save(ques);
        //questionsRepository.saveAndFlush(ques);
    }



    @Override
    public void deleteQuestion(long id) {
        var question = questionsRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException("");
        });
        questionsRepository.deleteById(id);
    }

    @Override
    public List<ru.csu.springweb.model.Poll> findPollsByDate(LocalDate start, LocalDate end) {
        System.out.println(start);
        var polls = pollRepository.findAll();
        var filteredPolls = new ArrayList<ru.csu.springweb.entity.Poll>();

        polls.forEach(poll -> {
            if (poll.getStartDate().isAfter(start) && poll.getEndDate().isBefore(end)) {
                filteredPolls.add(poll);
            }
        });

        return filteredPolls.stream().map(poll -> ru.csu.springweb.model.Poll
                .builder()
                .id(poll.getId())
                .pollName(poll.getPollName())
                .active(poll.getActive())
                .startDate(poll.getStartDate())
                .endDate(poll.getEndDate())
                .build()).collect(Collectors.toList());


    }
}
