package ru.csu.springweb.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.csu.springweb.dto.Poll;
import ru.csu.springweb.dto.PollFlt;
import ru.csu.springweb.service.PollService;
import ru.csu.springweb.dto.Qst;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/polls")
public class PollController {

    private final PollService pollService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public void createPoll(@RequestBody @Valid Poll poll) {
        pollService.createPoll(poll);
    }

    @GetMapping("/{id}")
    public ru.csu.springweb.model.Poll getPollById(@PathVariable long id) {
        return pollService.getPoll(id);
    }

    @GetMapping("search")
    public List<ru.csu.springweb.model.Poll> searchPollsByFilter(@RequestParam String startDate, @RequestParam String endDate) {
        return pollService.findPollsByDate(LocalDate.parse(startDate), LocalDate.parse(endDate));
    }

    @DeleteMapping("{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void deletePoll(@PathVariable long id) {
        pollService.deletePoll(id);
    }

    @PostMapping("{pollId}/questions")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void addQuestionToPoll(@PathVariable long pollId, @RequestBody Qst question) {
        pollService.addQuestionToPoll(pollId, question);
    }

}
