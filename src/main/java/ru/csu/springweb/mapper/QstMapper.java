package ru.csu.springweb.mapper;

import org.mapstruct.Mapper;
import ru.csu.springweb.dto.Qst;
import ru.csu.springweb.entity.Question;

@Mapper(componentModel = "spring")
public interface QstMapper extends AbstractMapper<Question, Qst> {

}

