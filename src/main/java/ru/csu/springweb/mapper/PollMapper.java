package ru.csu.springweb.mapper;

import org.mapstruct.Mapper;
import ru.csu.springweb.dto.Poll;

@Mapper(componentModel = "spring")
public interface PollMapper extends AbstractMapper<Poll, ru.csu.springweb.entity.Poll> {

}
