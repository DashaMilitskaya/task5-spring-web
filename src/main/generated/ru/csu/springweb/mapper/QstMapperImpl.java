package ru.csu.springweb.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import ru.csu.springweb.dto.Qst;
import ru.csu.springweb.dto.Qst.QstBuilder;
import ru.csu.springweb.entity.Question;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-05-18T20:26:18+0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 20 (Oracle Corporation)"
)
@Component
public class QstMapperImpl implements QstMapper {

    @Override
    public Qst mapSource(Question arg0) {
        if ( arg0 == null ) {
            return null;
        }

        QstBuilder qst = Qst.builder();

        qst.question( arg0.getQuestion() );
        qst.number( arg0.getNumber() );

        return qst.build();
    }

    @Override
    public Question mapTarget(Qst arg0) {
        if ( arg0 == null ) {
            return null;
        }

        Question question = new Question();

        question.setQuestion( arg0.getQuestion() );
        question.setNumber( arg0.getNumber() );

        return question;
    }

    @Override
    public List<Question> mapTarget(List<Qst> arg0) {
        if ( arg0 == null ) {
            return null;
        }

        List<Question> list = new ArrayList<Question>( arg0.size() );
        for ( Qst qst : arg0 ) {
            list.add( mapTarget( qst ) );
        }

        return list;
    }

    @Override
    public List<Qst> mapSource(List<Question> arg0) {
        if ( arg0 == null ) {
            return null;
        }

        List<Qst> list = new ArrayList<Qst>( arg0.size() );
        for ( Question question : arg0 ) {
            list.add( mapSource( question ) );
        }

        return list;
    }
}
