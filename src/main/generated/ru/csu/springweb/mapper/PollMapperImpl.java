package ru.csu.springweb.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import ru.csu.springweb.entity.Poll;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-05-17T13:38:08+0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 20 (Oracle Corporation)"
)
@Component
public class PollMapperImpl implements PollMapper {

    @Override
    public Poll mapSource(ru.csu.springweb.dto.Poll source) {
        if ( source == null ) {
            return null;
        }

        Poll poll = new Poll();

        poll.setPollName( source.getPollName() );
        poll.setStartDate( source.getStartDate() );
        poll.setEndDate( source.getEndDate() );
        poll.setActive( source.getActive() );

        return poll;
    }

    @Override
    public ru.csu.springweb.dto.Poll mapTarget(Poll target) {
        if ( target == null ) {
            return null;
        }

        ru.csu.springweb.dto.Poll poll = new ru.csu.springweb.dto.Poll();

        poll.setPollName( target.getPollName() );
        poll.setStartDate( target.getStartDate() );
        poll.setEndDate( target.getEndDate() );
        poll.setActive( target.getActive() );

        return poll;
    }

    @Override
    public List<ru.csu.springweb.dto.Poll> mapTarget(List<Poll> target) {
        if ( target == null ) {
            return null;
        }

        List<ru.csu.springweb.dto.Poll> list = new ArrayList<ru.csu.springweb.dto.Poll>( target.size() );
        for ( Poll poll : target ) {
            list.add( mapTarget( poll ) );
        }

        return list;
    }

    @Override
    public List<Poll> mapSource(List<ru.csu.springweb.dto.Poll> source) {
        if ( source == null ) {
            return null;
        }

        List<Poll> list = new ArrayList<Poll>( source.size() );
        for ( ru.csu.springweb.dto.Poll poll : source ) {
            list.add( mapSource( poll ) );
        }

        return list;
    }
}
