package ru.csu.springweb.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class Poll {

    Long id;

    String pollName;

    LocalDate startDate;

    LocalDate endDate;

    Boolean active;

    List<Object> questions;
}
