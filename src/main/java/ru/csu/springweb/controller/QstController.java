package ru.csu.springweb.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.csu.springweb.service.PollService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/questions")
public class QstController{
    private final PollService pollService;
    @DeleteMapping("{questionId}")
    public void deleteQuestionFromPoll(@PathVariable long questionId) {
        pollService.deleteQuestion(questionId);
    }
}

