package ru.csu.springweb.repository;

import org.springframework.data.repository.CrudRepository;
import ru.csu.springweb.entity.Question;

public interface QstRepository extends CrudRepository<Question, Long>{
    Question saveAndFlush(Question question);
}


